<?php 

namespace App\Repositories;

use App\App;
use App\Result;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;

class AppRepository {

	const apps = [
		'madivas-win-big',
		'truth-about-you',
		'dash-me-money',
	];
	protected $fb, $app, $result;

	public function __construct(App $app, Facebook $fb){
		$this->fb = $fb;
		$this->app = $app;
		// Image::configure(array('driver' => 'imagick'));
	}

	protected function fbGet($query){
		try {
	        $response = $this->fb->get($query);
	    } catch (\Facebook\Exceptions\FacebookSDKException $e) {
	        throw new \Exception("We would not reach facebook at the moment. Please try again later",404);
	    }
	    return $response->getDecodedBody();
	}

	public function processRequest()
	{
		\Debugbar::info($this->app);
		if (method_exists($this, camel_case($this->app->name))) {
			return $this->{camel_case($this->app->name)}();
		}
	}

	public function madivasWinBig()
	{
		$fbuDetails =$this->fbGet('me?fields=id,name,email');
		$fbProfilePic = $this->fbGet('me/picture?type=large&redirect=false');
		$resultImage = Image::make(storage_path("templates/".$this->app->name.".jpg"));
		$dp = Image::make($fbProfilePic['data']['url']);
		$resultImage->insert($dp,'top-left',66,185);
		$resultImage->text($fbuDetails['id'],653,220,function($font){
			$font->file(public_path('fonts/consolab.ttf'));
			$font->size(48);
			$font->color('#ffffff');
			$font->align('left');
		    $font->valign('top');
		});
		$filename = str_random(20).".jpg";
		$resultImage->save(public_path('img/results/'.$this->app->name.'/'.$filename));

		$result = new Result;
		$result->filename = $filename;
		$result->app_id = $this->app->id;
		Auth::user()->results()->save($result);
		return redirect()->route('result',[$this->app->id, $result->id]);
	}

	public function truthAboutYou()
	{
		$fbu = $this->fbGet('/me?fields=id,name,birthday,email,first_name,last_name,picture.width(200)');
		$birthday = new Carbon($fbu['birthday']);
		$resultImage = Image::make(storage_path("templates/".$this->app->name.".jpg"));
		$dp = Image::make($fbu['picture']['data']['url']);
		$resultImage->insert($resultImage, 'top-left', 60, 60);

		$textArea = Image::canvas(980, 720);
		$textArea->text($fbu['name'],0,20);
		$resultImage->insert($textArea,'top-right',0,0);
		// $resultImage->text(__('madivas.fortune.'.$birthday->format('F')),300,60, function($font){
		// 	$font->file(public_path('fonts/Roboto-Medium.ttf'));
		// 	$font->size(36);
		// 	$font->color('#555555');
		// 	$font->align('left');
		//     $font->valign('top');
		// });
		return $resultImage->response();


	}
}
