<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	public $incrementing = false;

    protected $casts = [
    	'extra' => 'object'
    ];

	public static function boot()
	{
	    parent::boot();
	    static::creating(function($table)
	    {
	        $table->id = $table->id ? $table->id : strtolower(str_random(20));
	    });
	}

    public function app()
    {
    	return $this->belongsTo(App::class);
    }

    public function owner()
    {
    	return $this->belongsTo(User::class,'owner_id');
    }
}
