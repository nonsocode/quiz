<?php

namespace App\Http\Controllers;

use App\App;
use App\Repositories\AppRepository;
use App\Result;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;

class AppsController extends Controller
{
    public function show(App $app)
    {
    	$apps = App::where('id','<>',$app->id)->get();

    	$this->og->title(env('APP_NAME')." | ".$app->name)
    		->description($app->description)
    		->image(appImg($app),[
    			"width" => 1200,
    			"height" => 628
			])
    		->url(route('app.show',$app->id));

    	return view('app.show', [
    		'app' => $app,
    		'apps' => $apps,
    		'og' => $this->og
    	]);
    }

    public function start(App $app, Facebook $fb)
    {
    	if (session()->has('fb_access_token') && !session('fb_access_token')->isExpired() && Auth::check()) {
    		$fb->setDefaultAccessToken(session('fb_access_token'));
    		return $this->initApp($app,$fb);
    	}
    	else{
			$helper = $fb->getRedirectLoginHelper();

			$permissions = ['email','public_profile','user_friends', 'read_custom_friendlists', 'user_birthday', 'user_likes']; 
			$loginUrl = $helper->getLoginUrl(route('callback',$app->id), $permissions);

	    	return redirect($loginUrl);    		
    	}
    }

	public function callback(App $app, Facebook $fb)
	{
		$fb =  $this->Authx2($fb);
		return $this->initApp($app,$fb);
	}

	protected function Authx2($fb){
		$helper = $fb->getRedirectLoginHelper();
		$token = $helper->getAccessToken();
		$oAuth2Client = $fb->getOAuth2Client();
		if (! $token->isLongLived()) {
	        try {
	            $token = $oAuth2Client->getLongLivedAccessToken($token);
	        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
				return back()->with('error','Could not reach facebook');
	        }
	    }
	    $fb->setDefaultAccessToken($token);
		session(['fb_access_token' => $token]);
		try {
	        $response = $fb->get('/me?fields=id,name,email');
	    } catch (\Facebook\Exceptions\FacebookSDKException $e) {
	        throw new Exception("We would not reach facebook at the moment. Please try again later",404);
	    }

	    $user = User::createOrUpdateGraphNode($response->getGraphUser());

	    Auth::login($user);
	    return $fb;
	}

	public function initApp($app, $fb)
	{
		$appRepo = new AppRepository($app, $fb);
		return $appRepo->processRequest();
		$appRepo->getResult();
		$appRepo->getResultImageUrl();
	}

	public function result(App $app, Result $result)
	{
    	$apps = App::where('id','<>',$app->id)->get();

    	$this->og->title(env('APP_NAME')." | ".$app->name)
    		->description($app->description)
    		->image(asset('img/results/'.$app->name.'/'.$result->filename),[
    			"width" => 1200,
    			"height" => 628
			])
    		->url(route('result',[$app->id,$result->id]));

		return view('result.show',[
    		'app' => $app,
    		'apps' => $apps,
    		'og' => $this->og,
    		'result' => $result
		]);
	}
}
