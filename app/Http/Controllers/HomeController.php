<?php

namespace App\Http\Controllers;

use App\App;
use App\User;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;

class HomeController extends Controller
{
    
    public function index()
    {
    	$apps = App::paginate();
    	return view('index',['apps'=>$apps,'og' => $this->og]);
    }

}
