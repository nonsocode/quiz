<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
		// parent::__construct();
    	$this->og = new \OpenGraph;
    	$this->og->title(env('APP_NAME'))
	        ->type('article')
	        ->description(env('APP_DESCRIPTION'))
	        ->url()
	        ->locale('en_NG')
	        ->localeAlternate(['en_US'])
	        ->siteName(env('APP_NAME'))
	        ->determiner('an');

	    if (request()->is('/')) {
	    	$this->og->image(asset('img/default.png'),[
	        	"type" => "image/png",
	        	"width" => 1200,
	        	"height" => 628,
        	]);
	    }
    }
}
