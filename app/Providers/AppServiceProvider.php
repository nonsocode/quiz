<?php

namespace App\Providers;

use App\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use SammyK\LaravelFacebookSdk\LaravelPersistentDataHandler;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(env('DB_DEFAULT_STRING_LENGTH',191));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Facebook\PersistentData\PersistentDataInterface', LaravelPersistentDataHandler::class);
    }
}
