<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class App extends Model
{
	use Sluggable;
	public $incrementing = false;

	public static function boot()
	{
	    parent::boot();

	    static::creating(function($table)
	    {
	        $table->id = $table->id ? $table->id : strtolower(str_random(20));
	    });
	}

	public function sluggable()
    {
        return [
            'name' => [
                'source' => 'title'
            ]
        ];
    }
}
