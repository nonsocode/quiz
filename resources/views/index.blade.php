@extends('layouts.master')

@section('main')
    <div class="container">
        <section id="all-apps">
            <div class="row">
                @foreach ($apps as $app)
                    <div class="col-md-4 app-card">
                        <a href="{{route('app.show',[$app->id])}}">
                            <div class="card card-white rounded">
                                <div class="app-image rounded">
                                    <img src="{{appImg($app)}}" class="img-responsive" alt="App+Image">
                                </div>
                                <div class="app-title">
                                    {{$app->title}}
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection