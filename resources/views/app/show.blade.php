@extends('layouts.master')

@section('main')
	<div class="container">
		<section>
			<div class="row">
				<div class="col-md-8 main-content">
					<div class="card rounded card-white">
						<div class="app-title text-center">
							<h1>{{$app->title}}</h1>
						</div>
                        <div class="app-image text-center">
                        	<a href="{{ route('app.start',$app->id) }}">
	                            <img src="{{appImg($app)}}" class="img-responsive" alt="App+Image">
                        	</a>
                        </div>
						<div class="app-description">
							{{$app->description}}
						</div>
						<div class="app-facebook text-center">
							<a href="{{ route('app.start',$app->id) }}" class="facebook-button"><i class="fa fa-facebook-square"></i> Continue with Facebook</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 visible-md visible-lg aside hide">
					@foreach ($apps as $app)
	                    <div class="col-md-12 app-card">
	                        <a href="{{route('app.show',[$app->id])}}">
	                            <div class="card card-white rounded">
	                                <div class="app-image rounded">
	                                    <img src="{{appImg($app)}}" class="img-responsive" alt="App+Image">
	                                </div>
	                                <div class="app-title">
	                                    {{$app->title}}
	                                </div>
	                            </div>
	                        </a>
	                    </div>
					@endforeach
                    <div class="blend"></div>
				</div>
			</div>
		</section>
		<section class="all-apps">
			<div class="row">
				@foreach ($apps as $app)
                    <div class="col-md-3 app-card">
                        <a href="{{route('app.show',[$app->id])}}">
                            <div class="card card-white rounded">
                                <div class="app-image rounded">
                                    <img src="{{appImg($app)}}" class="img-responsive" alt="App+Image">
                                </div>
                                <div class="app-title">
                                    {{$app->title}}
                                </div>
                            </div>
                        </a>
                    </div>
				@endforeach
			</div>
		</section>
	</div>
@endsection

@section('script')
	@include('inc.hide')
@endsection