<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css">
	<title>Page Not Found</title>
</head>
<body>
	<h1 class="face">:(</h1>
	<h4>We See that you are quite adventurous. Unfortunately, this path you are taking would lead you to nowhere.</h4>
	<a href="#" class="back" onclick="history.back()">Go Back</a>
	<style type="text/css">
	*{
		text-align: center;
		font-family: "Varela Round", "Arial Rounded", sans-serif;
		color : #707B86;
	}
	html{
		background-color: rgba(0,0,0,1);
		background-image:url(/img/404.jpg);
		background-size: cover;
	}
	.face{
		transform: rotateZ(90deg);
		line-height: 0;
		font-size: 7em;
	}
	h4{
		font-size: 2em;
		padding: 0 20px;
	}
	</style>
</body>
</html>