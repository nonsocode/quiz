@extends('layouts.master')


@section('head')
	@if (Auth::guest() || (Auth::check() && Auth::user()->id != $result->owner->id))
		<script type="text/javascript">
			window.location = "{{ route('app.show',$app->id) }}";
		</script>
	@endif	
@endsection

@section('main')
	<div class="container">
		<section>
			<div class="row">
				<div class="col-md-8 main-content">
					<div class="card rounded card-white">
						<div class="app-title text-center">
							<h1>{{$app->title}}</h1>
						</div>
                        <div class="app-image text-center">
	                        <a href="#" onclick="share()">
	                            <img src="{{asset('img/results/'.$app->name.'/'.$result->filename)}}" class="img-responsive" alt="App+Image">
                            </a>
                        </div>
						<div class="app-description">
							{{$app->description}}
						</div>
						<div class="app-facebook text-center">
							<a href="#" onclick="share()" class="facebook-button"><i class="fa fa-facebook-square"></i> Share on Facebook</a>
						</div>
						<div class="play-again">
							<a href="{{ route('app.show' , $app->id) }}">Play again</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 visible-md visible-lg aside hide">
					@foreach ($apps as $app)
	                    <div class="col-md-12 app-card">
	                        <a href="{{route('app.show',[$app->id])}}">
	                            <div class="card card-white rounded">
	                                <div class="app-image rounded">
	                                    <img src="{{appImg($app)}}" class="img-responsive" alt="App+Image">
	                                </div>
	                                <div class="app-title">
	                                    {{$app->title}}
	                                </div>
	                            </div>
	                        </a>
	                    </div>
					@endforeach
					<div class="blend"></div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('script')
	<script type="text/javascript">
		function share(){
			FB.ui({
			  method: 'share',
			  href: location.href,
			  hashtag : "#{{$app->hashtag ? studly_case($app->hashtag) : "MadivasGameShow"}}",
			  // quote : "I just played the Madivas Win big game to stand a chance to win ₦50,000.",
			}, 
			function(response){
				console.log(response);
			});
		}
	</script>
	@include('inc.hide')
@endsection