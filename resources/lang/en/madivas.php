<?php 
return [

	'madivas-win-big' => 'Congrats. Remember to share this picture on facebook to complete the game.',
	'fortune' => [
		'January' => "You are a self-willed and courageous person. Adventurous and full of life, you are clever, don’t let anybody tell you differently. Your impulsiveness is what sets you up to achieve great things because even though you can be impulsive you have clarity of thought and that sets you on a road to great success.",
		'February' => "You have a warm heart and this sets you apart from the crowd. You are trustworthy and helpful and this characteristics is what would open doors to greatness. Your warmth is what makes you an excellent partner and friend.",
		'March' => "You are spontaneous and this is why people love you and it’s even more so because you are quick witted and fun to be around. You are also intelligent and you love life. Your ability to take charge and communicate properly is what sets you aside from the lot.",
		'April' => "You are loving, your nature is caring but you can be cautious and this is what makes you an excellent person. Your imaginative and artistic side makes you among one of the few good human beings and an excellent friend.",
		'May' => "You are tough because you have to be but you are generally open-minded and generous. You love to take everyone along because you are a born leader. People might see you as egotistic but it’s because they are not as in control as you are. You are a real King",
		'June' => "You are smart, analytical and you are patient. You are an absolute purist and you posses unidirectional talents which helps you make the best decisions. Your life never spirals because you are incredibly intelligent and you are always in control.",
		'July' => "You are balanced, and brave hearted. You see the positivity in life because you are reasonable and thoughtful. You are never ruffled, no matter what you face you always keep your cool. You are a fighter and you always see the worlds in a positive light and this builds you up for greatness because your determination cannot be stopped.",
		'August' => "You are passionate with a magnetic personality, yes you are forceful with your decisions but that’s because you have to take charge. Your clarity of thoughts and expressions sets you aside as a very clever person. This smart’s is what takes you places and would open doors for you." ,
		'September'=> "You are philosophical and intelligent, you are also fun and you are the life of the party. Your optimism makes you see things in a lighter perspective and that’s what drives you to aspire to greater things. Nothing can stop you, reach for your goals.",
		'October' => "You are practical and ambitious. Your nature does not allow you to give up easily and this sets you on a vigilant part. Your tolerance and perseverance is one of your greatest qualities although you might be understand but what is clear is the fact that no matter how tough a situation is you never back down from a fight.",
		'November' => "You are an independent thinker with great originality, this deep thinking qualities is what’s makes you great and loyal friends. Your actions stems from great thoughts which makes you stand out as a very intelligent person.",
		'December' => "You are sensitive and sympathetic and you go out of your way to help those who are dear to you. This makes you the best and most wonderful person to hang around. This characteristics makes you different in a good way and it makes people want to go all out for you and this helps you on your way to greatness."
	],
	'truth-about-you' => "We bet you didn't know that about yourself. Pat yourself on the back because you deserve it &#128521;. Don't forget to share this on facebook."

];