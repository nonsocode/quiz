<?php

use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('try', function(LaravelFacebookSdk $fb) {
    dd($fb->getJavaScriptHelper()->getAccessToken());
});
Route::get('/', 'HomeController@index');
Route::group(['prefix' => 'apps'], function() {
    Route::get('{app}/','AppsController@show')->name('app.show');
    Route::get('{app}/start', 'AppsController@start')->name('app.start');
	Route::get('{app}/callback', "AppsController@callback")->name('callback');
	Route::get('{app}/result/{result}', "AppsController@result")->name('result');
});
