<?php

use App\App;
use Illuminate\Database\Seeder;

class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App::create([
        	'title' => "Madivas Win Big",
        	'description' => "Want To win some money, Play this game and stand a chance to win ₦50,000"
    	]);
        App::create([
        	'title' => "3 friends that would dash me money",
        	'description' => "You've got lots of friends but these ones are the most likely to give you money"
    	]);
        App::create([
        	'title' => "What is the truth about you",
        	'description' => "Want to find out more about yorself, Play this game and discover your hidden truths and potential"
    	]);
    }
}
